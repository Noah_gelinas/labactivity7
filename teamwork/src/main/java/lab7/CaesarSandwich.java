package lab7;

public class CaesarSandwich extends VegetarianSandwich{
    private String filling;

    public CaesarSandwich() {
        filling="Caesar dressing";
    }

    @Override
    public String getProtein() {
        return "Anchovies";
    }

    public String getFilling() {
        return filling;
    }

    @Override
    public Boolean isVegetarian() {
        return false;
    }
}
