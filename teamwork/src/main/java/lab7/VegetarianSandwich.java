package lab7;
public abstract class VegetarianSandwich implements ISandwich {
    private String filling;

    public VegetarianSandwich() { 
        this.filling = "";
    }

    public void addFilling(String topping) {
        String[] meat = new String[]{"chicken", "beef", "fish", "meat", "pork"};
        
        for (int i = 0; i < meat.length; i++) {
            if (topping == meat[i]) {
                throw new UnsupportedOperationException();
            }
            else {
                this.filling += topping + " ";
            }
        }
    }

    public String getFilling() {
        return this.filling;
    }

    public Boolean isVegetarian() {
        return true;
    }

    public Boolean isVegan() {
        Boolean vegan = true;
        if (filling.contains("cheese") || filling.contains("egg")) {
            vegan = false;
        }
        return vegan;
    }

    public abstract String getProtein();

}