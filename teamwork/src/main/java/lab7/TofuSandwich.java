package lab7;

public class TofuSandwich extends VegetarianSandwich {
    private String filling;

    public TofuSandwich() {
        this.filling = "Tofu";
    }

    public String getFilling() {
        return this.filling;
    }

    @Override
    public String getProtein() {
        return "Tofu";
    }
}
