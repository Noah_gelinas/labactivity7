package lab7;

public class BagelSandwich implements ISandwich{
    private String filling;

    public BagelSandwich(){
        this.filling="";
    }   

    public void addFilling(String topping) {
        this.filling+=topping+ " ";
    }

    public String getFilling() {
        return this.filling;
    }

    public Boolean isVegetarian() {
        throw new UnsupportedOperationException();
    }
}
