package lab7;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class VegetarianSandwichTest {
    
    @Test
    public void ConstructorTest() 
    {        
        VegetarianSandwich newVeg=new CaesarSandwich();
        assertTrue(true);
        VegetarianSandwich newVeg2=new TofuSandwich();
        assertTrue(true);   
    }

    @Test
    public void testAddFilling() {
        VegetarianSandwich newVeg=new CaesarSandwich();
        VegetarianSandwich newVeg2=new TofuSandwich();       
         newVeg.addFilling("lettuce");
         newVeg2.addFilling("lettuce");
    }

    @Test
    public void testGetFilling() {
        VegetarianSandwich newVeg=new CaesarSandwich();
        VegetarianSandwich newVeg2=new TofuSandwich();       
        newVeg.getFilling();
        newVeg2.getFilling();
    }

    @Test
    public void isVegetarianTest() {
        VegetarianSandwich newVeg=new CaesarSandwich();
        VegetarianSandwich newVeg2=new TofuSandwich();        
        newVeg.isVegetarian();
        newVeg2.isVegetarian();
    }

    @Test
    public void isVeganTest() {
        VegetarianSandwich newVeg=new CaesarSandwich();
        VegetarianSandwich newVeg2=new TofuSandwich();
        newVeg.addFilling("lettuce");
        assertTrue(newVeg.isVegan());
        newVeg2.addFilling("lettuce");
        assertTrue(newVeg2.isVegan());
    }
}
